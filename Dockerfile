FROM python:3.6
WORKDIR /app
RUN pip install Flask Flask-SocketIO eventlet
COPY . /app
EXPOSE 5000
CMD python -u app.py