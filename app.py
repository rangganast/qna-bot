from flask import Flask, render_template, url_for
from flask_socketio import SocketIO, send
import re
import json


app = Flask(__name__)
app.config['SECRET_KEY'] = 'hRzGr2LUGp'
socketio = SocketIO(app, cors_allowed_origins='*')


def get_similarity(text1, text2):
	text1 = re.sub(r'[^\w]', '', text1)
	text2 = re.sub(r'[^\w]', '', text2)

	text1 = re.sub(r'\d+', '', text1)
	text2 = re.sub(r'\d+', '', text2)
	
	text1_list = [word for word in text1.lower().split(' ')]
	text2_list = [word for word in text2.lower().split(' ')]

	l1 =[]
	l2 =[]

	text1_set = set(text1_list)  
	text2_set = set(text2_list)

	rvector = text1_set.union(text2_set)

	for w in rvector: 
		if w in text1_set: l1.append(1) # create a vector 
		else: l1.append(0) 
		if w in text2_set: l2.append(1) 
		else: l2.append(0)

	c = 0

	for i in range(len(rvector)): 
		c += l1[i]*l2[i]

	denom = float((sum(l1)*sum(l2))**0.5)
	if denom == 0:
		cosine = 0
	else:
		cosine = c / denom

	return cosine


@app.route('/')
def home():
	return render_template("index.html")


@socketio.on('message')
def handleMessage(msg):
	qna = json.load(open("qna.txt"))

	question = ''
	highest_cosine = 0

	for key, value in qna.items():
		cosine = get_similarity(msg, key)

		if cosine > highest_cosine:
			highest_cosine = cosine
			question = key

	if highest_cosine == 0:
		reply_msg = "Sorry, I can't really understand your question."
	else:
		reply_msg = qna[question]

	send(reply_msg, broadcast=True)


if __name__ == '__main__':
	socketio.run(app, host='0.0.0.0')